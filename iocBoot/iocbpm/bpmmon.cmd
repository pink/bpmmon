#!../../bin/linux-x86_64/bpm

#- You may have to change bpm to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/bpm.dbd"
bpm_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

dbLoadTemplate("rec.substitutions")

set_savefile_path("${TOP}/iocBoot/${IOC}/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "TOP=$(TOP),BL=PINK,DEV=BPM")

## Start any sequence programs
#seq sncxxx,"user=epics"
